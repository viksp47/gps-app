package com.gps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class GpsApiApplication {

	@RequestMapping(value = "/test")
	public String test(){
		return "success";
	}
	
	@RequestMapping(value = "/secure/test")
	public String secureTest(){
		return "success";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(GpsApiApplication.class, args);
	}
}
