package com.gps.base.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.gps.security.dto.GPSUserDTO;

/**
 * Abstract Controller.
 * 
 * @author vpatel
 *
 */
public abstract class BaseController {

	/**
	 * The method is responsible for providing UserId of the logged In user.
	 * 
	 * @return Long.
	 */
	protected Long getLoggedInUserId() {
		GPSUserDTO gpsUserDTO = getPrincipal();
		return gpsUserDTO.getUserId();
	}

	/**
	 * This method is responsible for providing principle object of
	 * authentication.
	 * 
	 * @return iceUserDTO.
	 */
	protected GPSUserDTO getPrincipal() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		return ((GPSUserDTO) auth.getPrincipal());
	}

}
