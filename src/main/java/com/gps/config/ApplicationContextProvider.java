package com.gps.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author vpatel.
 */
@Component
public class ApplicationContextProvider implements ApplicationContextAware {

	/**
	 * ApplicationContext.
	 */
	private static ApplicationContext applicationContext;

	/**
	 * Set the ApplicationContext.
	 */
	@Override
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		applicationContext = appContext;

	}

	/**
	 * @return the applicationContext.
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

}
