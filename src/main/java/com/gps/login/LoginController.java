package com.gps.login;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gps.security.component.GPSAuthenticationProvider;

@RestController
public class LoginController {

	@Autowired
	private GPSAuthenticationProvider authenticationProvider;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = { "application/json" })
	public void login(@RequestBody LoginDTO loginDTO, HttpServletRequest request){
		String username = loginDTO.getUsername();
		String password = loginDTO.getPassword();
		if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)){
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
			Authentication authentication = authenticationProvider.authenticate(token);
			SecurityContext securityContext = SecurityContextHolder.getContext();
		    securityContext.setAuthentication(authentication);
		}
	}
	
}
