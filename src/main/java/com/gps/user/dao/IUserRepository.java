package com.gps.user.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gps.user.db.User;

/**
 * This interface is responsible for crud and other operation for user.
 * 
 * @author vpatel
 */
public interface IUserRepository extends JpaRepository<User, Long> {

	/**
	 * This query method is responsible for search functionality of user by
	 * userName with paging.
	 * 
	 * @param userName
	 *            String.
	 * @param pageable
	 *            Pageable.
	 * @return User
	 */
	Page<User> findByUserNameLike(String userName, Pageable pageable);

	/**
	 * This query method is responsible for search functionality of user by
	 * userName and email id with paging.
	 * 
	 * @param userName
	 *            String.
	 * @param pageable
	 *            Pageable.
	 * @param emailId
	 *            of User.
	 * @return User
	 */
	Page<User> findByUserNameLikeOrEmailIdLike(String userName, String emailId,
			Pageable pageable);

	/**
	 * This method is resposible for get user by name.
	 * 
	 * @param userName
	 *            String
	 * @return User
	 */
	User findByUserName(String userName);

	/**
	 * This method is responsible for find user by email id.
	 * 
	 * @param emailId
	 *            String
	 * @return User
	 */
	User findByEmailId(String emailId);
}
