package com.gps.user.service.impl;

import java.time.LocalDateTime;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.gps.base.service.impl.AGenericsService;
import com.gps.user.dao.IUserRepository;
import com.gps.user.db.User;
import com.gps.user.service.IUserService;

/**
 * UserServiceImpl.
 * 
 * @author vpatel
 *
 */
@Service
@Transactional
public class UserServiceImpl extends AGenericsService<User, Long> implements
		IUserService {

	@Autowired
	private IUserRepository userRepository;
	
	@Override
	public JpaRepository<User, Long> getJpaRepository() {
		return userRepository;
	}
	
	@Autowired 
	private BCryptPasswordEncoder endcode;

	@PostConstruct
	public void run(){
		User user = new User("vikas",endcode.encode("password"),LocalDateTime.now(),"viksp47@gmail.com",9737561264L,true);
		this.userRepository.save(user);
	}
}