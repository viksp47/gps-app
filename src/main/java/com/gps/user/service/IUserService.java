package com.gps.user.service;

import com.gps.base.service.IGenericeService;
import com.gps.user.db.User;

/**
 * This class is for UserService.
 * 
 * @author vpatel
 *
 */
public interface IUserService extends IGenericeService<User, Long> {}
