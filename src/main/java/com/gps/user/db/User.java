/**
 * Package for user.
 */
package com.gps.user.db;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This entity is for user.
 * 
 * @author vpatel
 *
 */

@Entity(name = "T_GPS_USER_MASTER")
public final class User {

	/**
	 * userId.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private long userId;

	/**
	 * username.
	 */
	@Column(name = "username")
	private String userName;

	@Column(name = "password")
	private String password;

	/** The Joining Date of User. */
	@Column(name = "created_date")
	private LocalDateTime createdDate;

	/**
	 * Provide user mailId.
	 */
	@Column(name = "email_id")
	private String emailId;

	@Column(name = "contact_number")
	private long contactNumber;

	@Column(name = "is_admin")
	private boolean isAdmin;

	/**
	 * @return the userId.
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param id
	 *            the id to set.
	 */
	public void setUserId(final long id) {
		this.userId = id;
	}

	/**
	 * @return the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param name
	 *            . the name to set
	 */
	public void setUserName(final String name) {
		this.userName = name;
	}

	/**
	 * @return the emailId.
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the isAdmin
	 */
	public boolean getIsAdmin() {
		return isAdmin;
	}

	/**
	 * @param isAdmin
	 *            the isAdmin to set
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public User(String userName, String password, LocalDateTime createdDate, String emailId, Long contactNumber,
			boolean isAdmin) {
		super();
		this.userName = userName;
		this.password = password;
		this.createdDate = createdDate;
		this.emailId = emailId;
		this.contactNumber = contactNumber;
		this.isAdmin = isAdmin;
	}

	public User() {
		super();
	}

}
