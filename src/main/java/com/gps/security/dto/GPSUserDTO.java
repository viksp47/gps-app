package com.gps.security.dto;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class GPSUserDTO implements UserDetails {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 4554852507988808746L;

	/**
	 * userId.
	 */
	private Long userId;

	/**
	 * password.
	 */
	private String password;

	/**
	 * userName.
	 */
	private String username;

	/**
	 * emailId.
	 */
	private String emailId;

	/**
	 * userDescription.
	 */
	private String userDescription;

	/**
	 * authorities.
	 */
	private Set<GrantedAuthority> authorities;

	/**
	 * accountNonExpired.
	 */
	private boolean accountNonExpired;

	/**
	 * accountNonLocked.
	 */
	private boolean accountNonLocked;

	/**
	 * credentialsNonExpired.
	 */
	private boolean credentialsNonExpired;

	/**
	 * enabled.
	 */
	private boolean enabled;

	/**
	 * 
	 * @param userId
	 *            userId
	 * @param username
	 *            username
	 * @param password
	 *            password
	 * @param enabled
	 *            enabled
	 * @param accountNonExpired
	 *            accountNonExpired.
	 * @param credentialsNonExpired
	 *            credentialsNonExpired.
	 * @param accountNonLocked
	 *            accountNonLocked.
	 * @param authorities
	 *            authorities.
	 * @param userDescription
	 *            userDescription.
	 * @param emailId
	 *            String.
	 */
	public GPSUserDTO(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities,
			String userDescription, Long userId, String emailId) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.accountNonExpired = accountNonExpired;
		this.credentialsNonExpired = credentialsNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.authorities = new HashSet<>(authorities);
		this.userDescription = userDescription;
		this.userId = userId;
		this.emailId = emailId;
	}

	/**
	 * @return the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the username.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the authorities.
	 */
	public Set<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/**
	 * @param authorities
	 *            the authorities to set.
	 */
	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	/**
	 * @return the accountNonExpired.
	 */
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	/**
	 * @param accountNonExpired
	 *            the accountNonExpired to set.
	 */
	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	/**
	 * @return the accountNonLocked.
	 */
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	/**
	 * @param accountNonLocked
	 *            the accountNonLocked to set.
	 */
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	/**
	 * @return the credentialsNonExpired.
	 */
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	/**
	 * @param credentialsNonExpired
	 *            the credentialsNonExpired to set.
	 */
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	/**
	 * @return the enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the userDescription.
	 */
	public String getUserDescription() {
		return userDescription;
	}

	/**
	 * @param userDescription
	 *            the userDescription to set.
	 */
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	/**
	 * @return the userId.
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set.
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the emailId.
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
