package com.gps.security.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gps.security.dto.GPSUserDTO;
import com.gps.user.dao.IUserRepository;
import com.gps.user.db.User;

/**
 * Authentication Service required by spring security.
 * 
 * @author vpatel
 */
@Service
public class UserAuthenticationService implements UserDetailsService {

	/**
	 * Reference to Domain Manager Dao.
	 */
	@Autowired
	private IUserRepository userRepositoryImpl;

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		User user = userRepositoryImpl.findByUserName(userName);
		if (user == null) {
			throw new UsernameNotFoundException("No such user: " + userName);
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
		authorities.add(authority);

		if (user.getIsAdmin()) {
			authority = new SimpleGrantedAuthority("ROLE_ADMIN");
			authorities.add(authority);
		}

		return new GPSUserDTO(user.getUserName(), user.getPassword(), true, true,
				true, true, authorities, "discription", user.getUserId(), user.getEmailId());
	}
}
